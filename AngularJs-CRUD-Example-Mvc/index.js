//Required Modules
const express = require('express');
var morgan = require("morgan");

// Crear express app
var app = express();

// Configurar puerto de servidor
const port = process.env.PORT || 3200;

app.use(morgan("dev"));
app.use(express.static("./"));

app.get("/", function(req, res) {
    res.sendFile("./index.html");
    // Archivo index.html de tu aplicación AngularJs
});

// Start Server
app.listen(port, function () {
    console.log( "Express server listening on port " + port);
});
