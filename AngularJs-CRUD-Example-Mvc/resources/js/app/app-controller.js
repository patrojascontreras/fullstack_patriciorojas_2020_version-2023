'use strict';
myApp.controller('crudExampleController', ['$scope', 'personsService',  function($scope, personsService) {
    
    $scope.title_persons_management_gral = 'CRUD Gestión de Personas';
	$scope.title_persons_management_list = 'Listado de Personas';
	
	$scope.button_title_new_person_create = 'Nueva Persona';
	$scope.button_title_person_search = 'Buscar';
	
	$scope.grid_table_person_id = 'ID';
	$scope.grid_table_person_name = 'Nombre';
	$scope.grid_table_person_lastName = 'Apellidos';
	$scope.grid_table_person_male = 'Sexo';
	$scope.grid_table_person_email = 'Email';
	$scope.grid_table_person_category = 'Categoría';
	$scope.grid_table_person_actions = 'Acciones';

    $scope.searchingId = null;

    $scope.personIdDelete = 0;

    //Variables formulario Registro de Persona
    $scope.formPersonCreate = {
        name: '', 
        lastName: '', 
        male: '', 
        email: '', 
        category: 0
    };
    //Fin variables formulario Registro de Persona

    //Variables formulario Edición de Persona
    $scope.formPersonEdit = {
        idPerson: 0,
        name: '', 
        lastName: '', 
        male: '', 
        email: '', 
        category: 0
    };

    $scope.idPersonAntidisabled = 0;
    //Fin variables formulario Edición de Persona

    $scope.formPersonCancelBtn = 'Cancelar';
    $scope.formPersonCreateBtn = 'Registrar';
    $scope.formPersonUpdateBtn = 'Modificar';
    $scope.formPersonDeleteBtn = 'Eliminar';
    $scope.formPersonDetailsCloseBtn = 'Cerrar';

    $scope.titleFormPersonCreate = 'Formulario Registro Persona';
    $scope.titleFormPersonUpdate = 'Formulario Edición Persona';
    $scope.titleFormPersonDelete = 'Confirmar Eliminar';
    $scope.titleFormPersonDetails = 'Detalles de Persona';

    $scope.textDescriptionModalDeletePerson = '¿Desea eliminar este registro?';

    $scope.formPersonId = 'ID';
    $scope.formPersonName = 'Nombre';
    $scope.formPersonLastName = 'Apellidos';
    $scope.formPersonMale = 'Sexo';
    $scope.formPersonEmail = 'Email';
    $scope.formPersonCategory = 'Categoría';

    $scope.messageResponse = '';
    $scope.errorMessageResponse = '';

    //Variables respuestas de campos y/o combobox inválidos
    $scope.errorMessageInvalidName = 'Por favor, ingrese un nombre válido';
    $scope.errorMessageInvalidLastName = 'Por favor, ingrese un apellido(s) válido';
    $scope.errorMessageInvalidEmail = 'Por favor, ingrese un email válido';

    $scope.errorMessageMinLengthName = 'El campo Nombre debe tener al menos 2 letras';
    $scope.errorMessageMinLengthLastName = 'El campo Apellido(s) debe tener al menos 2 letras';
    //Fín Variables respuestas de campos y/o combobox inválidos

    $scope.prsListAll = [];
    $scope.catsListAll = [];

    $scope.personsListAll = function() {
        personsService.readPersonsListAll().then(function success(response) {
            $scope.prsListAll = response.data;
            console.log($scope.prsListAll);
            console.log("Status Code: " + response.status);
        }, function error(response) {

            console.log("Status Code: " + response.status);

            if(response.status == 404) {
                $scope.prsListAll = [];
                $scope.messageResponse = response.data.message;
                console.log($scope.messageResponse);
            } else if(response.status == 500) {
                $scope.prsListAll = [];
                $scope.errorMessageResponse = response.data.message;
                toastr.error($scope.errorMessageResponse, "Operación erronea");
                console.log($scope.errorMessageResponse);
            }
        });
    };

    $scope.personsListAllById = function() {
        console.log("Variable: " + $scope.searchingId);

        personsService.readPersonsListAllById($scope.searchingId).then(function success(response) {
            $scope.prsListAll = response.data;
            console.log($scope.prsListAll);

            $scope.searchingId = null;
        }, function error(response) {

            if(response.status == 404) {
                $scope.prsListAll = [];
                $scope.messageResponse = response.data.message;
                console.log($scope.messageResponse);
                $scope.searchingId = null;
            } else if(response.status == 500) {
                $scope.prsListAll = [];
                $scope.errorMessageResponse = response.data.message;
                toastr.error($scope.errorMessageResponse, "Operación erronea");
                console.log($scope.errorMessageResponse);
                $scope.searchingId = null;
            }
        });
    };

    $scope.categoriesListAll = function() {
        personsService.readCategoriesListAll().then(function success(response) {
            $scope.catsListAll = response.data;
            console.log($scope.catsListAll);
        }, function error(response) {

            if(response.status == 404) {
                $scope.catsListAll = [];
                $scope.messageResponse = response.data.message;
                toastr.error($scope.messageResponse, "Operación erronea");
                console.log($scope.messageResponse);
            } else if(response.status == 500) {
                $scope.catsListAll = [];
                $scope.errorMessageResponse = response.data.message;
                toastr.error($scope.errorMessageResponse, "Operación erronea");
                console.log($scope.errorMessageResponse);
            }
        });
    };

    $scope.openFormPersonCreate = function() {
        $scope.clearInputsFormPersonCreate();
        $scope.categoriesListAll();
        $('#modalFormPersonCreate').modal('show');
    };

    $scope.cancelPersonCreate = function() {
        $('#modalFormPersonCreate').modal('hide');
    };

    $scope.personCreate = function() {

        var personInsert = {
            name: $scope.formPersonCreate.name, 
            lastName: $scope.formPersonCreate.lastName, 
            male: $scope.formPersonCreate.male, 
            email: $scope.formPersonCreate.email, 
            personCategory: $scope.formPersonCreate.category
        };

        personsService.personCreate(personInsert).then(function success(response) {

            if(response.status == 201) {
                $scope.messageResponse = 'Dato registrado éxitosamente';
                
                $scope.personsListAll();
                toastr.success($scope.messageResponse, "Operación éxitosa");
                console.log($scope.messageResponse);
                $('#modalFormPersonCreate').modal('hide');
            } else if(response.status == 200) {
                console.log($scope.messageResponse);
                $scope.errorMessageResponse = response.data.message;
                toastr.error($scope.errorMessageResponse, "Operación erronea");
                console.log($scope.errorMessageResponse);
            }
        }, function error(response) {

            $scope.errorMessageResponse = response.data.message;
            toastr.error($scope.errorMessageResponse, "Operación erronea");
            console.log(response.status);
            console.log($scope.errorMessageResponse);
        });
    };

    $scope.clearInputsFormPersonCreate = function() {
        $scope.formPersonCreate.name = '';
        $scope.formPersonCreate.lastName = '';
        $scope.formPersonCreate.male = '';
        $scope.formPersonCreate.email = '';
        $scope.formPersonCreate.category = 0;

        $scope.disabledGetName = true;
        $scope.disabledGetLastName = true;
        $scope.disabledGetEmail = true;
    };

    $scope.personEdit = function(idPerson) {

        personsService.readPersonEdit(idPerson).then(function success(response) {
            console.log(response.data);

            $scope.disabledGetName = true;
            $scope.disabledGetLastName = true;
            $scope.disabledGetEmail = true;

            $scope.formPersonEdit.idPerson = response.data.idPerson;
            $scope.formPersonEdit.name = response.data.name;
            $scope.formPersonEdit.lastName = response.data.lastName;
            $scope.formPersonEdit.male = response.data.male;
            $scope.formPersonEdit.email = response.data.email;
            $scope.formPersonEdit.category = "" + response.data.personCategory + "";

            $scope.idPersonAntidisabled = response.data.idPerson;

            $scope.categoriesListAll();
            $('#modalFormPersonEdit').modal('show');
        }, function error(response) {

            if(response.status == 404) {
                $scope.messageResponse = response.data.message;
                toastr.error($scope.messageResponse, "Operación erronea");
                console.log($scope.messageResponse);
            } else if(response.status == 500) {
                $scope.errorMessageResponse = response.data.message;
                toastr.error($scope.errorMessageResponse, "Operación erronea");
                console.log($scope.errorMessageResponse);
            }
        });
    };

    $scope.personUpdate = function() {

        var personUpdated = {
            idPerson: $scope.idPersonAntidisabled,
            name: $scope.formPersonEdit.name, 
            lastName: $scope.formPersonEdit.lastName, 
            male: $scope.formPersonEdit.male, 
            email: $scope.formPersonEdit.email, 
            personCategory: $scope.formPersonEdit.category
        };

        personsService.personUpdate(personUpdated).then(function success(response) {

            console.log(response.data);

            $scope.messageResponse = 'Dato modificado éxitosamente';

            $scope.personsListAll();
            toastr.success($scope.messageResponse, "Operación éxitosa");
            console.log($scope.messageResponse);
            $('#modalFormPersonEdit').modal('hide');
        }, function error(response) {

            if(response.status == 404) {
                $scope.messageResponse = response.data.message;
                toastr.error($scope.messageResponse, "Operación erronea");
                console.log($scope.messageResponse);
            } else if(response.status == 500) {
                $scope.errorMessageResponse = response.data.message;
                toastr.error($scope.errorMessageResponse, "Operación erronea");
                console.log($scope.errorMessageResponse);
            }
        });
    };

    $scope.cancelPersonUpdate = function() {
        $('#modalFormPersonEdit').modal('hide');
    };

    $scope.openModalPersonDelete = function(idPerson) {
        $scope.personIdDelete = idPerson;
        console.log($scope.personIdDelete);
        $('#modalFormPersonDelete').modal('show');
    };

    $scope.confirmPersonDelete = function() {

        personsService.personDelete($scope.personIdDelete).then(function success(response) {

            console.log(response.data);

            $scope.messageResponse = 'Dato eliminado éxitosamente';

            $scope.personsListAll();
            toastr.success($scope.messageResponse, "Operación éxitosa");
            console.log($scope.messageResponse);
            $('#modalFormPersonDelete').modal('hide');
        }, function error(response) {

            if(response.status == 404) {
                $scope.messageResponse = response.data.message;
                toastr.error($scope.messageResponse, "Operación erronea");
                console.log($scope.messageResponse);
            } else if(response.status == 500) {
                $scope.errorMessageResponse = response.data.message;
                toastr.error($scope.errorMessageResponse, "Operación erronea");
                console.log($scope.errorMessageResponse);
            }
        });
    };

    $scope.cancelPersonDelete = function() {
        $('#modalFormPersonDelete').modal('hide');
    };

    $scope.openModalPersonDetails = function(idPerson) {
        personsService.readPersonDetails(idPerson).then(function success(response) {

            $scope.personDetails = response.data;
            console.log($scope.personDetails);
            $('#modalFormPersonReadDetails').modal('show');
        }, function error(response) {

            if(response.status == 404) {
                $scope.messageResponse = response.data.message;
                toastr.error($scope.messageResponse, "Operación erronea");
                console.log($scope.messageResponse);
            } else if(response.status == 500) {
                $scope.errorMessageResponse = response.data.message;
                toastr.error($scope.errorMessageResponse, "Operación erronea");
                console.log($scope.errorMessageResponse);
            }
        });
    };

    $scope.closeModalPersonDetails = function() {
        $('#modalFormPersonReadDetails').modal('hide');
    };

    //Funciones para validar determinados campos
    var namePattern = /^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ\s]+$/;
    var lastNamePattern = /^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ\s]+$/;
    var emailPattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    $scope.disabledGetName = true;
    $scope.disabledGetLastName = true;
    $scope.disabledGetEmail = true;

    $scope.disabledInvalidPatternName = true;
    $scope.disabledInvalidPatternLastName = true;
    $scope.disabledInvalidPatternEmail = true;

    $scope.disabledMinlengthName = true;
    $scope.disabledMinLengthLastName = true;

    $scope.getInvalidName = function(text) {

        $scope.disabledGetName = false;

        if(text.length < 2) {
            $scope.disabledMinlengthName = false;
        } else {
            $scope.disabledMinlengthName = true;
        }

        if(!namePattern.test(text)) {
            $scope.disabledInvalidPatternName = false;
        } else {
            $scope.disabledInvalidPatternName = true;
            $scope.disabledGetName = true;
        }
    };

    $scope.getInvalidLastName = function(text) {

        $scope.disabledGetLastName = false;

        if(text.length < 2) {
            $scope.disabledMinlengthLastName = false;
        } else {
            $scope.disabledMinlengthLastName = true;
        }

        if(!lastNamePattern.test(text)) {
            $scope.disabledInvalidPatternLastName = false;
        } else {
            $scope.disabledInvalidPatternLastName = true;
            $scope.disabledGetLastName = true;
        }
    };

    $scope.getInvalidEmail = function(text) {

        $scope.disabledGetEmail = false;

        if(!emailPattern.test(text)) {
            $scope.disabledInvalidPatternEmail = false;
        } else {
            $scope.disabledInvalidPatternEmail = true;
            $scope.disabledGetEmail = true;
        }
    };
    //Fín funciones para validar determinados campos
    
}]);
