'use strict';
myApp.service('personsService', ['personsFactory', function(personsFactory) {
    
    var $this = this;

    $this.readPersonsListAll = function() {
        return personsFactory.readPersonsListAll();
    };

    $this.readPersonsListAllById = function(idPerson) {
        return personsFactory.readPersonsListAllById(idPerson);
    };

    $this.personCreate = function(person) {
        return personsFactory.personCreate(person);
    };

    $this.personUpdate = function(person) {
        return personsFactory.personUpdate(person);
    };

    $this.personDelete = function(idPerson) {
        return personsFactory.personDelete(idPerson);
    };

    $this.readPersonEdit = function(idPerson) {
        return personsFactory.readPersonEdit(idPerson);
    };

    $this.readPersonDetails = function(idPerson) {
        return personsFactory.readPersonDetails(idPerson);
    };

    $this.readCategoriesListAll = function() {
        return personsFactory.readCategoriesListAll();
    };

}]);
