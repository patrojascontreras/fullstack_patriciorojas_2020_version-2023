'use strict';
myApp.factory('personsFactory', ['$http', function($http) {

    var urlBase = 'http://localhost:8080/SpringBoot-CRUD-Example/api';

    var specificalUrl = {
        'urlReadPersonsListAll': urlBase + '/persons/read/innerjoin/list', 
        'urlPersonCreate': urlBase + '/persons/create',
        'urlPersonUpdate': urlBase + '/persons/update', 
        'urlPersonDelete': urlBase + '/persons/delete', 
        'urlReadPersonEdit': urlBase + '/persons/read/edit', 
        'urlReadPersonDetails': urlBase + '/persons/read/persondetails', 
        'urlReadCategoriesListAll': urlBase + '/categories/read/list'
    };
    
    var factory = {
        readPersonsListAll: readPersonsListAll, 
        readPersonsListAllById: readPersonsListAllById,
        personCreate: personCreate, 
        personUpdate: personUpdate, 
        personDelete: personDelete, 
        readPersonEdit: readPersonEdit, 
        readPersonDetails: readPersonDetails, 
        readCategoriesListAll: readCategoriesListAll
    };
    return factory;

    /*
    return {
        readPersonsListAll: readPersonsListAll, 
        readPersonsListAllById: readPersonsListAllById,
        personCreate: personCreate, 
        personUpdate: personUpdate, 
        personDelete: personDelete, 
        readPersonEdit: readPersonEdit, 
        readPersonDetails: readPersonDetails, 
        readCategoriesListAll: readCategoriesListAll
    };*/

    function readPersonsListAll() {
        var request = $http.get(specificalUrl.urlReadPersonsListAll);
        return request;
    }

    function readPersonsListAllById(idPerson) {
        var request = $http.get(specificalUrl.urlReadPersonsListAll + '/' + idPerson);
        return request;
    }

    function personCreate(person) {
        var request = $http.post(specificalUrl.urlPersonCreate, person);
        return request;
    }

    function personUpdate(person) {
        var request = $http.put(specificalUrl.urlPersonUpdate, person);
        return request;
    }

    function personDelete(idPerson) {
        var request = $http.delete(specificalUrl.urlPersonDelete + '/' + idPerson);
        return request;
    }

    function readPersonEdit(idPerson) {
        var request = $http.get(specificalUrl.urlReadPersonEdit + '/' + idPerson);
        return request;
    }

    function readPersonDetails(idPerson) {
        var request = $http.get(specificalUrl.urlReadPersonDetails + '/' + idPerson);
        return request;
    }

    function readCategoriesListAll() {
        var request = $http.get(specificalUrl.urlReadCategoriesListAll);
        return request;
    }

}]);
