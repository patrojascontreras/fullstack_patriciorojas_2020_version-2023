'use strict';
myApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider
    .when('/', {
        controller: 'crudExampleController', 
        templateUrl: 'templates/persons_management.html'
	})
	.otherwise({ 
        redirectTo: '/' 
    });
}]);
