# FullStack_PatricioRojas_2020_Version-2023



## Descripción

Proyecto de desarrollo web que consiste en un CRUD de datos vinculados a personas, que el cuál se ha realizado con herramientas como HTML5, CSS3 y JavaScript mediante librerías como JQuery y Toastr. A nivel de Frameworks en FrontEnd, se utilizaron Bootstrap y AngularJs, en la cuál se implementaron métodos de servicios como Factory y Service en conjunto, aunque ambos cumplan funciones diferentes dentro del proyecto.

### Diferencias entre Factory y Service: 

En AngularJS, estos dos métodos crean servicios que son objetos Singleton, por lo que persisten datos durante la vida útil de la aplicación web. Los cuáles se definen de la siguiente manera:

* **Factory:** Es un método que toma un nombre y una función que se inyecta mediante el retorno de un objeto literal.
* **Service:** Es un método que está definido mediante una función constructora usando this.

**Avisos importantes:**  

* En ambos métodos se pueden inyectar dependencias (DI) en su definición, pero no es configurable.
* Son esencialmente diferentes formas de producir el mismo resultado, creando un servicio singleton. Cuál prefieres usar depende en gran medida de tu estilo y los requisitos de tu proyecto específico.
* Con el proyecto mostrado, se puede definir que ambos tipos de servicios (**factory()** y **service()**) se pueden usar en conjunto. 

Proyecto fabricado con VS Code a nivel de IDE. 

Se usa y se realiza el despliegue con el Framework Node.Js en modo de servidor. 

Para mostrar el proyecto desplegado, se debe ingresar a la siguiente url: http://localhost:3200 

### Estructura del proyecto: 

![Estructura del proyecto](/Images/project_structure_frontend_angularjs_crud-example.jpg) 

El flujo se verá como a continuación

![Flujo del proyecto](/Images/flag_angularjs_project-crud-example.jpg) 

**Dentro de AngularJs, ¿por qué es tan importante el concepto Two-Way Data Binding?** 

Two-Way Data Binding es el concepto base de AngularJs, ya que fue un concepto **muy necesario cuando apareció**, y lo sigue siendo hasta el día de hoy. 

![Estructura Two-Way Data Binding](/Images/two-way_data_binding.png) 

Es un concepto realmente importante, ya que gracias a él **se sincroniza la información de la vista con la información que tenemos en el modelo o view model**, que es un modelo que tenemos en la parte del controlador.

Es decir, **todos los datos con los que trabaja el controlador están totalmente sincronizados con la vista, lo que nos permite modificar información en la vista y que se refleje totalmente en el modelo y al revés.**

Cada vez que modificamos algo en el controlador, se refleja en la vista y se pueden ver esos cambios.

Esto resulta algo muy potente, y sobre todo lo fue en su momento, ya que viniendo de marcos como jQuery, fue algo bastante innovador.

En las actuales y futuras versiones de Angular **el concepto de Two-Way Data Binding se ha ido ampliando**, y actualmente es un framework totalmente modular, orientado a componentes y mucho más potente. 

**Avisos importantes:** 

* Para la realización de este proyecto, se basó a traves del proyecto AngularJs-CRUD-Example, que el cual se encuentra en el repositorio: https://gitlab.com/patrojascontreras/fullstack_patriciorojas_2020
* Para la interacción del BackEnd, se utilizó un proyecto Java con Spring Boot mediante microservicios en API REST que se encuentra en el mismo repositorio en donde está el proyecto FrontEnd.
* En relación a AngularJs, tambien se están usando implementaciones como la directiva **ng-include** para incluir HTML de un archivo externo, y sin olvidar el bloque **config**, que el cuál es una función que se ejecuta al iniciar un programa en AngularJs tras haber realizado el despliegue del mismo proyecto.
* Los métodos de servicios Factory y Service se aprendieron entre julio y agosto de 2023 por mi parte, a pesar de poseer conocimientos en AngularJs desde julio de 2017, pero recién el año anterior por motivos de tiempo logré investigar y conocer esos métodos de una manera más detallada, a pesar de no haberlos usado en anteriores experiencias laborales, aunque haya usado el formato tradicional de ese Framework. 

Realizado por Ing. Patricio Rojas Contreras - 31 de agosto de 2023.
